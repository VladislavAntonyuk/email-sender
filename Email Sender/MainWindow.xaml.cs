﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;

namespace Email_Sender
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string GetString(RichTextBox rtb)
        {
            var textRange = new TextRange(rtb.Document.ContentStart, rtb.Document.ContentEnd);
            return textRange.Text;
        }

        void SetString(RichTextBox rtb, string text)
        {
            rtb.Document.Blocks.Clear();
            AppendString(rtb, text);
        }

        void AppendString(RichTextBox rtb, string text)
        {
            rtb.Document.Blocks.Add(new Paragraph(new Run(text)));
        }

        private void SetSelection(PasswordBox passwordBox, int start, int length)
        {
            passwordBox.GetType().GetMethod("Select", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(passwordBox, new object[] { start, length });
        }

        public async Task SendEmail(SmtpClient smtp, string toEmailAddress, string emailSubject, string emailMessage)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(loginTextBox.Text);
            // message.IsBodyHtml = true;
            message.Priority = MailPriority.High;
            message.To.Add(toEmailAddress);
            message.Subject = emailSubject;
            message.Body = emailMessage;
            // message.Attachments.Add(new Attachment("Path")) ;
            await smtp.SendMailAsync(message);
        }

        private async void sendBtn_Click(object sender, RoutedEventArgs e)
        {
            if (loginTextBox.Text.Length > 0 && passTextBox.Text.Length > 0 && MailListBox.Items.Count > 0 && GetString(MessageTextBox).Length > 0)
            {
                loginTextBox.IsEnabled = false;
                passTextBox.IsEnabled = false;
                try
                {
                    SmtpClient smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(loginTextBox.Text, passTextBox.Text)
                    };
                    for (int i = 0; i < MailListBox.Items.Count - 1; i++)
                    {
                        await SendEmail(smtp, MailListBox.Items[i].ToString(), "Subject", GetString(MessageTextBox));
                        this.Title = string.Format("Current: {0}, left: {1}", MailListBox.Items[i].ToString(), (MailListBox.Items.Count - 2 - i).ToString());
                    }
                    MessageBox.Show("Done");
                    loginTextBox.IsEnabled = true;
                    passTextBox.IsEnabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Input your login, password, message and load database");
            }
        }

        private void showPassCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            passwordBox.Visibility = Visibility.Collapsed;
            passTextBox.Visibility = Visibility.Visible;
            passTextBox.Focus();
        }

        private void showPassCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            passwordBox.Visibility = Visibility.Visible;
            passTextBox.Visibility = Visibility.Collapsed;
            SetSelection(passwordBox, passwordBox.Password.Length, 0);
            passwordBox.Focus();
        }
        
        private async void loadBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opn = new OpenFileDialog();
            if (opn.ShowDialog() == true)
            {
                MailListBox.ItemsSource = null;
                var lines = await ReadAllLinesAsync(opn.FileName);
                Emails = new ObservableCollection<string>();
                MailListBox.ItemsSource = Emails;
                await this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new LoadDelegate(Load), lines, 0);
            }
        }

        private void passwordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            passTextBox.Text = passwordBox.Password;
            passTextBox.CaretIndex = passTextBox.Text.Length;
        }

        public static Task<string[]> ReadAllLinesAsync(string path)
        {
            return ReadAllLinesAsync(path, Encoding.UTF8);
        }

        public static async Task<string[]> ReadAllLinesAsync(string path, Encoding encoding)
        {
            var lines = new List<string>();
            using (var reader = new StreamReader(path, encoding))
            {
                string line;
                while ((line = await reader.ReadLineAsync()) != null)
                {
                    lines.Add(line);
                }
            }
            return lines.ToArray();
        }

        private ObservableCollection<string> Emails;
        private delegate void LoadDelegate(string[] email, int i);

        private void Load(string[] email, int i)
        {
            if (i < email.Length)
            {
                Emails.Add(email[i].ToString());
                this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new LoadDelegate(Load), email, ++i);
            }
            else
            {
                MessageBox.Show("Done");
                sendBtn.IsEnabled = true;
            }
        }

    }
}
